﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.Json; // Remember to add a reference to System.Text.Json
// You may also have to download and install System.Threading.Tasks.Extensions (and maybe System.Runtime.CompilerServices.Unsafe ?)
// if your target does not have it. Use ILMerge to "statically link" (merge in)
// the assembly if required

// Collect environment data on a host that can be used to key payloads in the future
// Similar code has already been written here:
// https://github.com/FuzzySecurity/Sharp-Suite/tree/master/DiscerningFinch

// Tested on a Windows 10 VM. No guarantee this is going to work on Windows 7 or XP
namespace ek47Survey
{
    class Program
    {
        [DllImport("kernel32")]
        extern static UInt64 GetTickCount64();

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                Usage();
            }
            {
                Dictionary<string, string> env = Survey();
                Console.WriteLine();
                var envJson = JsonSerializer.Serialize(env);
                Console.WriteLine(envJson);
            }
        }
        static private void Usage()
        {
            Console.WriteLine("EK47Survey.exe -- Survey a host for some environment specific things");
            Console.WriteLine("Run without arguments");
        }

        // Let's give the option of keying on a few different things.
        // 1. Environment variables
        // 2. Folders inside "C:\Program Files (x86)"
        // 3. Boot time
        static private Dictionary<string, string> Survey()
        {
            Dictionary<string, string> env = new Dictionary<string, string>(); // result dictionary

            // Environment variables
            List<string> possibleKeys = new List<string>()
            {
                "username",
                "userdomain",
                "userprofile",
                "logonserver",
                "homepath",
                "appdata",
                "computername"
            };

            Random rand = new Random();
            int choices = rand.Next(2, 5); // How many environment variables we'll query

            for (int i = 0; i < choices; i++)
            {
                int index = rand.Next(possibleKeys.Count);
                string regkey = possibleKeys[index];
                possibleKeys.RemoveAt(index);
                string regval = Environment.GetEnvironmentVariable(regkey);
                env[regkey] = regval;

                Console.WriteLine(regkey + " : " + regval);
            }

            // Program Files
            choices = rand.Next(5, 9); // How many "Program Files (x86)" folders we'll query
            string[] subdirs = Directory.GetDirectories(@"C:\Program Files (x86)");

            for (int i = 0; i < choices; i++)
            {
                int index = rand.Next(subdirs.Length);
                string dirname = subdirs[index];
                env["Program" + index.ToString()] = dirname;

                Console.WriteLine("Program" + index.ToString() + " : " + dirname);
            }

            // Boot time
            DateTime bootTime = DateTime.Now - TimeSpan.FromMilliseconds(GetTickCount64());
            env["Boot"] = bootTime.ToUniversalTime().ToString();

            Console.WriteLine("Boot : " + env["Boot"]);

            return env;
        }
    }
}